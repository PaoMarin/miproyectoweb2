import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Estado } from './estado';

@Injectable()
export class EstadoService {

  constructor(private http: Http) { 
  }


read(query = ''){
	return this.http.get('http://localhost:8000/estados',{
		params: { q: query }
	});
}

insert(data: Estado){
	return this.http.post('http://localhost:8000/estados', data);
}

update(data: Estado){
	return this.http.put('http://localhost:8000/estados/'+data.id, data);
}

delete(id) {
    return this.http.delete('http://localhost:8000/estados/'+id);
  }

  findById(id) {
    return this.http.get('http://localhost:8000/estados/'+id);
  }

}
