import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Phone } from './phone';

@Injectable()
export class PhoneService {
  constructor(private http: Http) {
  }

  read(query = '') {
    return this.http.get('http://localhost:8000/phones', {
      params: { q: query }
    });
  }

  insert(data: Phone) {
    return this.http.post('http://localhost:8000/phones', data);
  }

  update(data: Phone) {
    return this.http.put('http://localhost:8000/phones/'+data.id, data);
  }

  delete(id) {
    return this.http.delete('http://localhost:8000/phones/'+id);
  }

  findById(id) {
    return this.http.get('http://localhost:8000/phones/'+id);
  }
}
