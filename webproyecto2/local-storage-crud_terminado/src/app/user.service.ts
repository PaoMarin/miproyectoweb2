import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { User } from './user';

@Injectable()
export class UserService {
  constructor(private http: Http) {
  }

  read(query = '') {
    return this.http.get('http://localhost:8000/users', {
      params: { q: query }
    });
  }

  insert(data: User) {
    return this.http.post('http://localhost:8000/users', data);
  }

  update(data: User) {
    return this.http.put('http://localhost:8000/users/'+data.id, data);
  }

  delete(id) {
    return this.http.delete('http://localhost:8000/users/'+id);
  }

  findById(id) {
    return this.http.get('http://localhost:8000/users/'+id);
  }
}
