import { Component, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cliente-detail',
  templateUrl: './cliente-detail.component.html',
  styleUrls: ['./cliente-detail.component.css']
})
export class ClienteDetailComponent implements OnInit {

  cliente: Cliente;
  private sub: any;
  constructor(private route: ActivatedRoute, private service: ClienteService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.service.findById(params['id']).subscribe(res => {
        this.cliente = res.json();
      });
    });
  }
}
