import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BoletasCrudComponent } from './boletas-crud/boletas-crud.component';
import { HomeComponent } from './home/home.component';
import { BoletaDetailComponent } from './boleta-detail/boleta-detail.component';

import { ClientesCrudComponent } from './clientes-crud/clientes-crud.component';
import { ClienteDetailComponent } from './cliente-detail/cliente-detail.component';
import { RepuestosCrudComponent } from './repuestos-crud/repuestos-crud.component';
import { EstadosCrudComponent } from './estados-crud/estados-crud.component';

const routes: Routes = [
  { path: '', component: BoletasCrudComponent },
  { path: 'boletas', component: BoletasCrudComponent },
  { path: 'boletas/:id', component: BoletaDetailComponent },
  { path: 'home', component: HomeComponent },
  { path: 'clientes', component: ClientesCrudComponent },
  { path: 'clientes/:id', component: ClienteDetailComponent },
  { path: 'repuestos', component: RepuestosCrudComponent },
  { path: 'estados', component: EstadosCrudComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
