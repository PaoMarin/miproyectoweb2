import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  user: User;
  private sub: any;
  constructor(private route: ActivatedRoute, private service: UserService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.service.findById(params['id']).subscribe(res => {
        this.user = res.json();
      });
    });
  }

}
