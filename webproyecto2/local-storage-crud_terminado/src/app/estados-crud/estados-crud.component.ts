import { Component, OnInit } from '@angular/core';
import { Estado } from '../estado';
import { EstadoService } from '../estado.service';

@Component({
  selector: 'app-estados-crud',
  templateUrl: './estados-crud.component.html',
  styleUrls: ['./estados-crud.component.css']
})
export class EstadosCrudComponent implements OnInit {

  data: Estado[];
  current_estado: Estado;
  crud_operation = { is_new: false, is_visible: false};
  query: string = '';
  constructor(private service: EstadoService) { }

  ngOnInit() {
    this.service.read(this.query).subscribe(res =>{
      this.data = res.json();
      this.current_estado = new Estado();
    });
  }

new() {
    this.current_estado = new Estado();
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }

  edit(row) {
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = false;
    this.current_estado = row;
  }

  delete(id) {
    this.service.delete(id).subscribe(res => {
      this.crud_operation.is_new = false;
      this.ngOnInit();
    });
  }

  save() {
    if (this.crud_operation.is_new) {
      this.service.insert(this.current_estado).subscribe(res => {
        this.current_estado = new Estado();
        this.crud_operation.is_visible = false;
        this.ngOnInit();
      });
      return;
    }
    this.service.update(this.current_estado).subscribe(res => {
      this.current_estado = new Estado();
      this.crud_operation.is_visible = false;
      this.ngOnInit();
    });
  }
  }