import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Repuesto } from './repuesto';

@Injectable()
export class RepuestoService {
  constructor(private http: Http) {
  }

  read(query = '') {
    return this.http.get('http://localhost:8000/repuestos', {
      params: { q: query }
    });
  }

  insert(data: Repuesto) {
    return this.http.post('http://localhost:8000/repuestos', data);
  }

  update(data: Repuesto) {
    return this.http.put('http://localhost:8000/repuestos/'+data.id, data);
  }

  delete(id) {
    return this.http.delete('http://localhost:8000/repuestos/'+id);
  }

  findById(id) {
    return this.http.get('http://localhost:8000/repuestos/'+id);
  }
}
