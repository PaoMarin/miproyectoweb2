import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BoletasCrudComponent } from './boletas-crud/boletas-crud.component';
import { HomeComponent } from './home/home.component';

import { BoletaService } from './boleta.service';
import { BoletaDetailComponent } from './boleta-detail/boleta-detail.component';
import { HttpModule } from '@angular/http';


import { ClientesCrudComponent } from './clientes-crud/clientes-crud.component';

import { ClienteService } from './cliente.service';
import { ClienteDetailComponent } from './cliente-detail/cliente-detail.component';
//import { UsersCrudComponent } from './users-crud/users-crud.component';
//import { UserDetailComponent } from './user-detail/user-detail.component';
import { RepuestoService } from './repuesto.service';
import { RepuestosCrudComponent } from './repuestos-crud/repuestos-crud.component';
import { EstadoService } from './estado.service';
import { EstadosCrudComponent } from './estados-crud/estados-crud.component';


@NgModule({
  declarations: [
    AppComponent,
    BoletasCrudComponent,
    HomeComponent,
    BoletaDetailComponent,
    ClientesCrudComponent,
    ClienteDetailComponent,
    //UsersCrudComponent,
   /// UserDetailComponent,
    RepuestosCrudComponent,
    EstadosCrudComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    BoletaService,
    ClienteService,
    RepuestoService,
    EstadoService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
