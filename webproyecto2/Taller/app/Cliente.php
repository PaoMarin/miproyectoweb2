<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'nombre', 'apellido', 'telefono', 'direccion', 'cedula'
    ];

    public static function search($query = '')
    {
      if (!$query) {
        return self::all();
      }
      return self::where('nombre', 'ilike', "%$query%")
                  ->orWhere('nombre', 'ilike', "%$query%")
                  ->orWhere('telefono', '=', intval($query))->get();
    }
}
