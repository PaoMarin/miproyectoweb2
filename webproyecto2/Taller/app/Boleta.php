<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boleta extends Model
{
    protected $fillable = [
          'estado_reparacion','fecha_ingreso','cliente', 'equipo', 'estado_llegada', 'imagen','fecha_salida','reparacion','costo_reparacion','tecnico_cargo','firma'
    ];

    public static function search($query = '')
    {
      if (!$query) {
        return self::all();
      }
      return self::where('estado_reparacion', 'ilike', "%$query%")
                  ->orWhere('fecha_ingreso', '=', dateval($query))->get()
                  ->orWhere('cliente', 'ilike', "%$query%")
                  ->orWhere('equipo', 'ilike', "%$query%")
                  ->orWhere('estado_llegada', 'ilike', "%$query%")
                  ->orWhere('imagen', 'ilike', "%$query%")
                  ->orWhere('fecha_salida', '=', dateval($query))->get()
                  ->orWhere('reparacion', 'ilike', "%$query%")
                  ->orWhere('costo_reparacion', '=', doubleval($query))->get()
                  ->orWhere('tecnico_cargo', 'ilike', "%$query%")
                  ->orWhere('firma', 'ilike', "%$query%");
    }
}