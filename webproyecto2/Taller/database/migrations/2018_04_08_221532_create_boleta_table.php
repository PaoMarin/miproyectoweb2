<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoletaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletas', function (Blueprint $table) {
            $table->increments('id');
             $table->string('estado_reparacion');
             $table->date('fecha_ingreso');
             $table->string('cliente');
             $table->string('equipo');
             $table->string('estado_llegada');
             $table->string('imagen');
             $table->date('fecha_salida');
             $table->string('reparacion');
             $table->double('costo_reparacion');
             $table->string('tecnico');
             $table->string('firma');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletas');
    }
}
