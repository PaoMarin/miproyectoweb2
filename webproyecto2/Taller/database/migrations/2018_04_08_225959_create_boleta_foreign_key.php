<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoletaForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('boletas', function (Blueprint $table) {
         $table->unsignedString('estado_reparacion');

            $table->foreign('estado_reparacion')->references('descripcion')->on('estados');
        });

    }

     public function up()
        {
          
           Schema::table('boletas', function (Blueprint $table) {
             $table->unsignedString('cliente');

            $table->foreign('cliente')->references('id')->on('clientes');
            });
        }

         public function up()
        {
          
           Schema::table('boletas', function (Blueprint $table) {
             $table->unsignedString('imagen');

            $table->foreign('cliente')->references('id')->on('clientes');
            });
        }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
